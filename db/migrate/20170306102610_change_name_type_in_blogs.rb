class ChangeNameTypeInBlogs < ActiveRecord::Migration[5.0]
  def change
    change_table :blogs do |t|
      t.remove :name
      t.string :name
    end
  end
end

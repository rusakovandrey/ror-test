# Controller for blogs
class BlogsController < ApplicationController
  def index
    @blogs = Blog.all
  end

  def show
    @blog = Blog.find(params[:id])
  end

  def new
    @blog = Blog.new
  end

  def edit
    @blog = Blog.find(params[:id])
  end

  def create
    @blog = Blog.new(blog_params)

    if @blog.save
      redirect_to @blog
    else
      render 'new'
    end
  end

  def update
    @blog = Blog.find(params[:id])

    if @blog.update(blog_params)
      redirect_to @blog
    else
      render 'edit'
    end
  end

  def destroy
    # There's a series of callbacks associated with destroy. If the before_destroy
    #   callback throws :abort the action is cancelled and destroy returns false.
    # There's a series of callbacks associated with destroy!. If the before_destroy
    #   callback throws :abort the action is cancelled and destroy! raises ActiveRecord::RecordNotDestroyed.
    @blog = Blog.find(params[:id])
    @blog.destroy!

    redirect_to blogs_path
  end

  private

  def blog_params
    params.require(:blog).permit(:name)
  end
end

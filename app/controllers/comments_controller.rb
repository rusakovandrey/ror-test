# Controller for comments of articles
class CommentsController < ApplicationController
  def create
    @blog = Blog.find(params[:blog_id])
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to blog_article_path(@blog, @article)
  end

  def destroy
    @blog = Blog.find(params[:blog_id])
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to blog_article_path(@blog, @article)
  end

  private

  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end
end

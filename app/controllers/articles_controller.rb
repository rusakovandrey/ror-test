# Controller for articles in blogs
class ArticlesController < ApplicationController
  def index
    @blog = Blog.find(params[:blog_id])
    @articles = @blog.articles.all
  end

  def show
    @blog = Blog.find(params[:blog_id])
    @article = Article.find(params[:id])
  end

  def new
    @blog = Blog.find(params[:blog_id])
    @article = @blog.articles.build
  end

  def edit
    @blog = Blog.find(params[:blog_id])
    @article = Article.find(params[:id])
  end

  def create
    @blog = Blog.find(params[:blog_id])
    @article = @blog.articles.build(article_params)

    if @article.save
      redirect_to blog_article_path(@blog, @article)
    else
      render 'new'
    end
  end

  def update
    @blog = Blog.find(params[:blog_id])
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to blog_article_path(@blog, @article)
    else
      render 'edit'
    end
  end

  def destroy
    @blog = Blog.find(params[:blog_id])
    @article = Article.find(params[:id])
    @article.destroy!

    redirect_to blog_articles_path(@blog)
  end

  private

  def article_params
    params.require(:article).permit(:title, :text)
  end
end

require 'faker'
FactoryGirl.define do
  factory :blog do |f|
    f.name { Faker::Name.name }
  end
end

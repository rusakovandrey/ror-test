require 'faker'
FactoryGirl.define do
  factory :user do |f|
    trait :random_email do
      f.email { Faker::Internet.email }
    end

    trait :random_password do
      f.password { Faker::Internet.password }
    end

    factory :user_random, traits: [:random_email, :random_password]
  end
end

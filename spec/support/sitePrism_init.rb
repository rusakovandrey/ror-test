Dir["#{Rails.root}/spec/pages/*.rb"].each { |file| require file }

class App
  def sign_in
    SignIn.new
  end

  def sign_up
    SignUp.new
  end
end

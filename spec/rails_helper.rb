# This file is copied to spec/ when you run 'rails generate rspec:install'

# sets environment as test if environment is not set. w/o that env = development. And will wipe dev DB
ENV['RAILS_ENV'] ||= 'test'

# require environment
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?

# require 'spec_helper'
require 'rspec/rails'
# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#

# require 'support/factory_girl'
require 'shoulda/matchers'
require 'database_cleaner'
require 'capybara/rspec'
require 'factory_girl_rails'
require_relative 'support/sitePrism_init'

# Set DatabaseCleaner strategy. Possible strategies: truncation (TRUNCATE TABLE w/o deleting of table structure),
#   deletion (DELETE FROM), transaction (use of BEGIN TRANSACTION and ROLLBACK)
DatabaseCleaner.strategy = :truncation

# Checks for pending migration and applies them before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

# Enable shoulda matchers. used for validate_presence_of at this moment in models/blog_spec
Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

# Capybara config goes here
# http://www.rubydoc.info/github/teamcapybara/capybara/master/Capybara#configure-class_method
Capybara.configure do |config|
  config.default_driver = :selenium
end

# Yields the global configuration to a block. e.g. config.fixture_path == RSpec.configuration.fixture_path
RSpec.configure do |config|
  # its default way of handling db in RSpec. similar strategy as transaction.
  #   Since we handle it with dbcleaner - we don't really need it
  config.use_transactional_fixtures = false

  # same as above, but clear DB after each test. :example == :each
  # :suite - before/after all groups of tests, :context | :all - before/after group of tests,
  #   :example | :each - before/after each test.
  # can add additional parameters e.g. around(:example, :type => :feature)
  config.around(:example, type: :feature) do |example|
    @app = App.new
    example.run
    DatabaseCleaner.clean
  end

  # include factory girl methods module
  config.include FactoryGirl::Syntax::Methods

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  # config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
end

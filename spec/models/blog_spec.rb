# require 'rails_helper'

RSpec.describe Blog, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.create(:blog)).to be_valid
  end

  it 'is invalid without name' do
    expect(FactoryGirl.build(:blog, name: nil)).not_to be_valid
  end

  it { should validate_presence_of(:name) }
end

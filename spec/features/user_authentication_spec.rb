RSpec.feature 'User Authentication' do
  scenario 'successfull sign up', jst: true do
    user = FactoryGirl.build(:user_random)

    @app.sign_up.success(user.email, user.password, user.password)
    expect(@app.sign_up.user_presented?(user.email)).to be true
  end

  scenario 'successfull sign in' do
    user = FactoryGirl.create(:user_random)

    @app.sign_in.success(user.email, user.password)
    expect(page).to have_text('Listing Blogs') # TODO: change to siteprims blog url
  end

  scenario 'incorrect password on login' do
    user = FactoryGirl.create(:user_random)

    @app.sign_in.success(user.email, user.password.to_s + '1')
    expect(page).to have_text('Invalid Email or password.') # TODO: move error messages to collection
  end
end

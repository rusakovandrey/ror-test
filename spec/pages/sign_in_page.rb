require_relative 'base_page'

class SignIn < BasePage
  set_url 'users/sign_in'

  element :email, "input[name='user[email]']"
  element :password, "input[name='user[password]']"
  element :confirm_button, 'input[name="commit"]'

  def success(email, password)
    load
    self.email.set(email)
    self.password.set(password)
    confirm_button.click
  end
end

class BasePage < SitePrism::Page
  def logout
    click_link('Logout')
    page.driver.browser.switch_to.alert.accept
  end

  def user_presented?(email)
    usr = User.find_by email: email
    !usr.nil?
  end
end

require_relative 'base_page'

class SignUp < BasePage
  set_url '/users/sign_up'

  element :email, "input[name='user[email]']"
  element :password, "input[name='user[password]']"
  element :password_confirmation, "input[name='user[password_confirmation]']"
  element :confirm_button, 'input[name="commit"]'

  def success(email, password, _confirmation)
    load
    self.email.set(email)
    self.password.set(password)
    password_confirmation.set(password)
    confirm_button.click
  end
end
